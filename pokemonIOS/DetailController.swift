//
//  DetailController.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Kingfisher

class DetailController: UIViewController {
    
    let SPEED_STAT : Int = 0
    let SPE_DEF_STAT : Int = 1
    let SPE_ATK_STAT : Int = 2
    let DEF_STAT : Int = 3
    let ATK_STAT : Int = 4
    let HP_STAT : Int = 5
    
    
    //Properties
    var pokemonUrl: String?
    var myPokemon: Pokemon?
    var myPokeFavorisArray : Array<Int> = UserDefaults.standard.array(forKey: "pokeFavoris") as! Array<Int>
    
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var specialDefenseLabel: UILabel!
    @IBOutlet weak var speciaAttackLabel: UILabel!
    @IBOutlet weak var defenseLabel: UILabel!
    @IBOutlet weak var attackLabel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var pokeImg: UIImageView!

    @IBAction func retourAction(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        let pokemonId : Int = myPokemon?.id ?? 0
        if(!myPokeFavorisArray.contains(pokemonId) && pokemonId != 0) {
            myPokeFavorisArray.append(pokemonId)
        }
        UserDefaults.standard.set(myPokeFavorisArray, forKey: "pokeFavoris")
        UserDefaults.standard.synchronize()
    }
    /**
            lorsque la vue est  complètement chargée on met à jour l'affichage par appel à l'api
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()
    }
    /**
     met à jour  les labels de la vue
     */
    private func getPokemon(){
        AF.request(pokemonUrl ?? "", method: .get).responseDecodable {
            (response: DataResponse<Pokemon, AFError>) in
            switch response.result {
            case .success(let pokemon):
                self.myPokemon = pokemon
                self.setUiElements(myPokemon: pokemon)
            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
    
    private func setUiElements(myPokemon: Pokemon) {
        let pokemonSprites : Sprites = myPokemon.sprites ?? Sprites(backDefault: "", frontDefault: "")
        let imgUrl = URL(string: pokemonSprites.frontDefault ?? "")
        let pokemonStats : [Stats] = myPokemon.stats ?? Array()

        speedLabel.text = "\(pokemonStats[SPEED_STAT].stat?.name ?? "") \(pokemonStats[SPEED_STAT].baseStat ?? 0)"
        specialDefenseLabel.text = "\(pokemonStats[SPE_DEF_STAT].stat?.name ?? "") \(pokemonStats[SPE_DEF_STAT].baseStat ?? 0)"
        speciaAttackLabel.text = "\(pokemonStats[SPE_ATK_STAT].stat?.name ?? "") \(pokemonStats[SPE_ATK_STAT].baseStat ?? 0)"
        defenseLabel.text = "\(pokemonStats[ATK_STAT].stat?.name ?? "") \(pokemonStats[ATK_STAT].baseStat ?? 0)"
        attackLabel.text = "\(pokemonStats[DEF_STAT].stat?.name ?? "") \(pokemonStats[DEF_STAT].baseStat ?? 0)"
        hpLabel.text = "\(pokemonStats[HP_STAT].stat?.name ?? "") \(pokemonStats[HP_STAT].baseStat ?? 0)"
        
        pokeImg.kf.setImage(with: imgUrl)
    }
    
}
