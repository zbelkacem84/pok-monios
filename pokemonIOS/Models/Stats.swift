//
//  Stat.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation

class Stats: Codable {
    let baseStat: Int?
    let stat: Element?

    enum CodingKeys: String, CodingKey {
        case baseStat = "base_stat"
        case stat
    }

    init(baseStat: Int?, stat: Element?) {
        self.baseStat = baseStat
        self.stat = stat
    }
}
