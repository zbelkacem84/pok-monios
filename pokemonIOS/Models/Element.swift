//
//  Stat.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation

class Element: Codable {
    let name: String?
    let url: String?

    init(name: String?, url: String?) {
        self.name = name
        self.url = url
    }
}
