//
//  Type.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation

class Type: Codable {
    let slot: Int?
    let type: Element?

    init(slot: Int?, type: Element?) {
        self.slot = slot
        self.type = type
    }
}
