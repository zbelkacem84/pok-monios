//
//  Sprites.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation

class Sprites: Codable {
    let backDefault: String?
    let frontDefault: String?

    enum CodingKeys: String, CodingKey {
        case backDefault = "back_default"
        case frontDefault = "front_default"
    }

    init(backDefault: String?, frontDefault: String?) {
        self.backDefault = backDefault
        self.frontDefault = frontDefault
    }
}
