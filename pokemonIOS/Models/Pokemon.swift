//
//  Pokemon.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation
import UIKit

class Pokemon: Codable {
    let id: Int?
    let name: String?
    let sprites: Sprites?
    let stats: [Stats]?
    var types: [Type]? = Array()

    enum CodingKeys: String, CodingKey {
        case id, name, sprites, stats, types
    }

    init(id: Int?, name: String?, sprites: Sprites?, stats: [Stats]?, types: [Type]?) {
        self.id = id
        self.name = name
        self.sprites = sprites
        self.stats = stats
        self.types = types
    }
    
    public func getTypesString() -> String {
        var typesString : String = ""
        if (types?.isEmpty == false) {
            for typeObject in types! {
                typesString = typesString + " " + (typeObject.type?.name ?? "")
            }
        }
        
        return typesString
    }
}
