//
//  PokedexResult.swift
//  pokemonIOS
//
//  Created by etudiant on 22/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import Foundation

class Pokedex: Codable {
    let count : Int?
    let next : String?
    let previous : String?
    let results : [Element]?

    init(count: Int, next: String?, previous: String?, results: [Element]?) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}
