//
//  PokemonTableViewCell.swift
//  pokemonIOS
//
//  Created by etudiant on 22/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class PokemonTableViewCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonTypeLabel: UILabel!
    @IBOutlet weak var pokemonNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /**
     * Permet de faire un appel à l'API pour le pokémon concerné et en cas de succès affiche les informations
     * sur les éléments de l'UI
     */
    func fill(url: String) {
        AF.request(url, method: .get).responseDecodable {
            (response: DataResponse<Pokemon, AFError>) in
            switch response.result {
            case .success(let pokemon):

                let pokemonSprites : Sprites = pokemon.sprites ?? Sprites(backDefault: "", frontDefault: "")
                let url = URL(string: pokemonSprites.frontDefault ?? "")
                let pokedexId : Int = pokemon.id ?? 0

                self.pokemonNameLabel.text = pokemon.name
                self.pokemonImageView.kf.setImage(with: url)
                self.pokemonTypeLabel.text = pokemon.getTypesString()
                self.pokemonNumberLabel.text = "#" + String(pokedexId)

            case .failure(let error):
                print(error.errorDescription ?? "")
            }
        }
    }
}
