//
//  ViewController.swift
//  pokemonIOS
//
//  Created by etudiant on 21/01/2020.
//  Copyright © 2020 NicoZak. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    /* Limite aux nombres de pokémons (les pokémons indexés 10000+
     * dans l'API ne sont pas utilisables)
     */
    let MAX_LIMIT : Int = 806
    
    // Nombre de pokémons à afficher sur l'application
    let SHOW_LIMIT : Int = 10
    
    //Pokémons à afficher
    var pokemonsToShow : [Element] = Array()
    var selectedPokemon: String?
    var isFavorite : Bool?
    var favPokemons : Array = Array<String>()
    
    let refreshControl = UIRefreshControl()
    
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var pokemonTableView: UITableView!
    
    @IBAction func favAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let listVC = storyboard.instantiateViewController(identifier: "listVC") as ViewController
        self.navigationController?.pushViewController(listVC, animated: true)
        listVC.isFavorite = true
        listVC.title = "PokeFavoris"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "PokeRandom"
        getPokedexData()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Récupération de pokémons...")
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        pokemonTableView.refreshControl = refreshControl
        pokemonTableView.delegate = self
        pokemonTableView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPokemon" {
            let detailVC = segue.destination as? DetailController
            detailVC?.pokemonUrl = selectedPokemon
        }
    }
    
    /**
     * Permet de récupérer la liste de tous les pokémons, la mélanger et prendre les 10 premiers
     * pour les afficher
     */
    private func getPokedexData() {
        if(isFavorite ?? false) {
            getPokemonsFav()
        }
        else {
            AF.request("https://pokeapi.co/api/v2/pokemon?limit=\(MAX_LIMIT)", method: .get).responseDecodable { [weak self] (response: DataResponse<Pokedex, AFError>) in
                    switch response.result {
                        case .success(let pokemons):
                            var allPokemons : [Element]? = pokemons.results
                            allPokemons?.shuffle()
                            self?.pokemonsToShow = Array((allPokemons?.prefix(self?.SHOW_LIMIT ?? 10)) ?? ArraySlice())
                            self?.pokemonTableView.reloadData()
                            self?.refreshControl.endRefreshing()
                        case .failure(let error):
                           print(error.errorDescription ?? "")
                    }
            }
        }
    }
    
    /**
     * Permet de récupérer la liste de tous les pokémons favoris
     */
    private func getPokemonsFav() {
        for pokemon in UserDefaults.standard.array(forKey: "pokeFavoris") ?? Array() {
            favPokemons.append("https://pokeapi.co/api/v2/pokemon/\(pokemon)/")
        }
    }
    
    @objc private func refreshData(_ sender: Any) {
        getPokedexData()
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPokemon = isFavorite ?? false ? favPokemons[indexPath.row] : pokemonsToShow[indexPath.row].url
        self.performSegue(withIdentifier: "showPokemon", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFavorite ?? false ? favPokemons.count : pokemonsToShow.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cellDynamique = tableView.dequeueReusableCell(withIdentifier: "pokemonCellID", for: indexPath) as? PokemonTableViewCell {
            
            let correctPokemon = isFavorite ?? false ? favPokemons[indexPath.row] : pokemonsToShow[indexPath.row].url
            cellDynamique.fill(url: correctPokemon ?? "")
            
            return cellDynamique
        }else {
            return UITableViewCell()
        }
    }
}
